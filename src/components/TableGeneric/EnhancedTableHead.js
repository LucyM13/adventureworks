import * as React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";

const EnhancedTableHead = (props) => {
    const { headRows, order, orderBy, onRequestSort } = props;
    const createSortHandler = property => event => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                { headRows.map(row => {
                    return (
                        <TableCell
                            key={ row.id }
                            sortDirection={ orderBy === row.id ? order : false }
                        >
                            <TableSortLabel
                                active={ orderBy === row.id }
                                direction={ order }
                                onClick={ createSortHandler(row.id) }
                            >
                                { row.label }
                            </TableSortLabel>
                        </TableCell>
                    )
                })}
            </TableRow>
        </TableHead>
    );
};

export default EnhancedTableHead;