import * as React from "react";
import TablePagination from "@material-ui/core/TablePagination";

const Pagination = (props) => {
    const { rowsPerPageOptions, count, rowsPerPage, page, onChangePage, onChangeRowsPerPage } = props;

    return (
        <TablePagination
            rowsPerPageOptions={ rowsPerPageOptions }
            component="div"
            count={ count }
            rowsPerPage={ rowsPerPage }
            page={ page }
            backIconButtonProps={{
                'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
                'aria-label': 'Next Page',
            }}
            onChangePage={ onChangePage }
            onChangeRowsPerPage={ onChangeRowsPerPage }
        />
    );
};

export default Pagination;