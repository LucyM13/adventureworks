import * as React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import styled from "styled-components";

const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 0.5;
    justify-content: center;
`;

const RowContainer = styled.div`
    display: flex;
    padding-bottom: 4rem;
    justify-content: center;
`;


const CustomerForm = (props) => {

    const { error, customer, cities, states, updateCustomer, handleSubmit, handleDelete } = props;

    const getCity = (cityId) => {
        return cities.find(city => {
            return (city.Id === cityId)
        });
    };

    const getState = (city) => {
        const state = states.find(state => {
            return (state.Id === city.StateId)
        });
        return state.Name;
    };

    return (
        <FormContainer>
            <form>
                <RowContainer>
                    <TextField
                        style={{ flexBasis: 400, paddingRight: "6rem" }}
                        label="Name"
                        value={ (customer.Name !== null && customer.Name !== undefined) ? customer.Name : "" }
                        onChange={ e => {
                            customer.Name = e.target.value;
                            updateCustomer(customer);
                        }}
                        required
                        error={ error }
                    />
                    <TextField
                        style={{ flexBasis: 400 }}
                        label="Surname"
                        value={ (customer.Surname !== null && customer.Surname !== undefined) ? customer.Surname : "" }
                        onChange={ e => {
                            customer.Surname = e.target.value;
                            updateCustomer(customer);
                        }}
                        required
                        error={ error }
                    />
                </RowContainer>
                <RowContainer>
                    <TextField
                        style={{ flexBasis: 400, paddingRight: "6rem" }}
                        label="Email"
                        value={ (customer.Email !== null && customer.Email !== undefined) ? customer.Email : "" }
                        onChange={ e => {
                            customer.Email = e.target.value;
                            updateCustomer(customer);
                        }}
                        required
                        error={ error }
                    />
                    <TextField
                        style={{ flexBasis: 400 }}
                        label="Telephone"
                        value={ (customer.Telephone !== null && customer.Telephone !== undefined) ? customer.Telephone : "" }
                        onChange={ e => {
                            customer.Telephone = e.target.value;
                            updateCustomer(customer);
                        }}
                        required
                        error={ error }
                    />
                </RowContainer>
                <RowContainer>
                    <TextField
                        style={{ flexBasis: 400, paddingRight: "6rem"  }}
                        label="State"
                        value={ customer.State }
                        disabled
                    />
                    <FormControl style={{ flexBasis: 400 }}>
                        <InputLabel htmlFor="city">City</InputLabel>
                        <Select
                            label="City"
                            value={ (customer.CityId !== null && customer.CityId !== undefined) ? customer.CityId : 0 }
                            onChange={ (e) => {
                                const city = getCity(e.target.value);
                                customer.CityId = city.Id;
                                customer.State = getState(city);
                                updateCustomer(customer);
                            }}
                        >
                            <MenuItem key={ 0 } value={ 0 }> None </MenuItem>
                            {
                                cities && cities.map(city => {
                                    return <MenuItem key={ city.Id } value={ city.Id }> { city.Name } </MenuItem>
                                })
                            }
                        </Select>
                    </FormControl>
                </RowContainer>
                <RowContainer style={{ paddingBottom: "1rem" }}>
                    <Button
                        style={{ backgroundColor: "#EB028D", color: "white", width: "2rem", flexBasis: "70%" }}
                        onClick={ handleSubmit }
                    >
                        {
                            handleDelete === null &&
                                <span>Add</span>
                        }
                        {
                            handleDelete !== null &&
                                <span>Save</span>
                        }
                    </Button>
                </RowContainer>
                {
                    handleDelete !== null &&
                    <RowContainer>
                        <Button
                            style={{ backgroundColor: "#3B5998", color: "white", width: "2rem", flexBasis: "70%" }}
                            onClick={ handleDelete }
                        >
                            Delete
                        </Button>
                    </RowContainer>
                }
            </form>
        </FormContainer>
    );
};

export default CustomerForm;