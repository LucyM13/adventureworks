import * as React from "react";
import { Link } from "@reach/router";
import CircleWithInitials from "../CircleWithInitials";
import GetConnectedUser from "../Utils/GetConnectedUser";
import "./Navigation.css"

const Navigation = () => {
    const routes = [
        {
            id: 0,
            name: "Dashboard",
            path: "/",
            anonymous: true,
            connected: false,
        },
        {
            id: 1,
            name: "Dashboard",
            path: "/",
            anonymous: false,
            connected: true,
        },
        {
            id: 2,
            name: "Login",
            path: "/signin",
            anonymous: true,
            connected: false,
        },
        {
            id: 3,
            name: "Logout",
            path: "/signout",
            anonymous: false,
            connected: true,
        },
        {
            id: 4,
            name: "Sign Up",
            path: "/signup",
            anonymous: null,
            connected: null,
        }
    ];

    const strToTitleCase = (str) => {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    };

    return (
        <nav className="sidenav">
            {
                GetConnectedUser() &&
                <div className="header">
                    <CircleWithInitials
                        username={ GetConnectedUser() }
                        size={ 75 }
                    />
                    <p style={{ fontSize: '20px' }}>
                        {
                            strToTitleCase(GetConnectedUser())
                        }
                    </p>
                </div>
            }
            {
                routes.map((route, index) => {
                    if (route.anonymous == null || route.connected === null ||
                        (GetConnectedUser() === null && route.connected === true) ||
                        (GetConnectedUser() !== null && route.anonymous === true)) {
                        return null;
                    }
                    return (
                        <Link
                            key={ index }
                            to={ route.path }
                            getProps={ ({ isCurrent }) => {
                                return { className: `navlink ${isCurrent && "active"}` };
                            } }
                        >
                            { route.name }
                        </Link>
                    );
                })
            }
        </nav>
    );
};

export default Navigation;