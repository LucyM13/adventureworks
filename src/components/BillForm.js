import * as React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import styled from "styled-components";
import AddItem from "./AddItem";

const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin: 2rem;
    justify-content: center;
`;

const RowContainer = styled.div`
    display: flex;
    padding-bottom: 4rem;
    justify-content: center;
`;


const BillForm = (props) => {

    const { error, bill, sellers, categories, subcategories, products, item, updateItem, updateBill, handleDeleteItem, handleSubmit } = props;

    const [ open, setOpen ] = React.useState(false);

    const accessItemForm = () => {
        const tmpItem = {
            Id: 0,
            Quantity: 0,
            Product: {
                Id: 0,
                Name: ""
            },
            Subcategory: {
                Id: 0,
                Name: ""
            },
            Category: {
                Id: 0,
                Name: ""
            },
            Price: 0
        };
        updateItem(tmpItem);
        setOpen(true);
    };

    const addItem = (item) => {
        const min = 1;
        const max = 100;
        const rand = min + Math.random() * (max - min);
        item.Id = rand;
        bill.Items.push(item);
        updateBill(bill);
        setOpen(false);
    };

    return (
        <FormContainer>
            <form>
                <RowContainer>
                    <TextField
                        style={{ flexBasis: 400, paddingRight: "6rem" }}
                        label="BillNumber"
                        value={ bill.BillNumber }
                        onChange={ e => {
                            bill.BillNumber = e.target.value;
                            updateBill(bill);
                        }}
                        required
                        error={ error }
                    />
                    <FormControl style={{ flexBasis: 400 }} error={ error }>
                        <InputLabel htmlFor="sellers">Sellers</InputLabel>
                        <Select
                            label="Sellers"
                            value={ bill.SellerId }
                            onChange={ (e) => {
                                bill.SellerId = e.target.value;
                                updateBill(bill);
                            }}
                        >
                            <MenuItem key={ 0 } value={ 0 }> None </MenuItem>
                            {
                                sellers && sellers.map(seller => {
                                    return <MenuItem key={ seller.Id } value={ seller.Id }> { seller.Name } { seller.Surname }</MenuItem>
                                })
                            }
                        </Select>
                    </FormControl>
                </RowContainer>

                <RowContainer>
                    {
                        bill.Items && bill.Items.map(item => {
                            return (
                                <Card key={ item.Id } style={{ margin: '1rem' }}>
                                    <CardContent>
                                        <div><strong>Category</strong> : <span style={{ float: 'right' }}>{ item.Category.Name }</span></div>
                                        <div><strong>Sub-Category</strong> : <span style={{ float: 'right' }}>{ item.Subcategory.Name }</span></div>
                                        <div><strong>Product Name</strong> : <span style={{ float: 'right' }}>{ item.Product.Name }</span></div>
                                        <div><strong>Quantity</strong> : <span style={{ float: 'right' }}>{ item.Quantity }</span></div>
                                        <div><strong>Price</strong> : <span style={{ float: 'right' }}>{ item.Price } HRK</span></div>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small" onClick={ () => handleDeleteItem(item) }> Delete </Button>
                                    </CardActions>
                                </Card>
                            );
                        })
                    }
                </RowContainer>

                <RowContainer style={{ paddingBottom: "1rem" }}>
                    <Button
                        style={{ backgroundColor: "#3B5998", color: "white", width: "2rem", flexBasis: "70%" }}
                            onClick={ accessItemForm }
                    >
                        Add item
                    </Button>
                </RowContainer>
                <RowContainer>
                    <Button
                        style={{ backgroundColor: "#EB028D", color: "white", width: "2rem", flexBasis: "70%" }}
                        onClick={ handleSubmit }
                    >
                        Add bill
                    </Button>
                </RowContainer>
                <AddItem
                    open={ open }
                    categories={ categories }
                    subcategories={ subcategories }
                    products={ products }
                    item={ item }
                    updateItem={ updateItem }
                    handleItem={ () => addItem(item) }
                    handleClose={ () => setOpen(false) } />
            </form>
        </FormContainer>
    );
};

export default BillForm;