import * as React from "react";
import GetConnectedUser from "../components/Utils/GetConnectedUser";
import PageNotAuthorized from "../pages/PageNotAuthorized";

const AuthenticationRequired = ({ children }) => {
    if (GetConnectedUser() === null) {
        return <PageNotAuthorized />
    }
    return children;
};

export default AuthenticationRequired;
