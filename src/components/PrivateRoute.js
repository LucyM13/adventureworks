import React from "react";
import Route from "./Route";
import AuthenticationRequired from "./AuthenticationRequired";

const PrivateRoute = ({ component, ...rest }) => (
    <AuthenticationRequired>
        <Route component={component} {...rest} />
    </AuthenticationRequired>
);

export default PrivateRoute;