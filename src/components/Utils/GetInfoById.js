const GetInfoById = (data, id) => {
    return data.find(obj => {
        return (obj.Id === id);
    });
};

export default GetInfoById;