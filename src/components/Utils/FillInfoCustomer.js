import GetInfoById from "./GetInfoById";

const FillInfoCustomer = (id, customers, cities, states) => {
    const customer = customers.find(customer => {
        return customer.Id.toString() === id;
    });
    const city = GetInfoById(cities, customer.CityId);
    if (city !== undefined) {
        customer["City"] = city.Name;
        const state = GetInfoById(states, city.StateId);
        if (state !== undefined)
            customer["State"] = state.Name;
    }
    return customer;
};

export default FillInfoCustomer;