import * as React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DialogTitle from "@material-ui/core/DialogTitle";
import GetInfoById from "../components/Utils/GetInfoById";
import styled from "styled-components";

const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 0.5;
    margin: 3rem 8rem 3rem 8rem;
`;

const RowContainer = styled.div`
    display: flex;
    padding-bottom: 2rem;
    justify-content: center;
`;

const AddItem = (props) => {
    const { open, categories, subcategories, products, item, updateItem, handleItem, handleClose } = props;

    const [ error, setError ] = React.useState(false);

    const changeQuantity = (e) => {
        item.Quantity = e.target.value;
        updateItem(item);
    };

    const changePrice = (e) => {
        item.Price = e.target.value;
        updateItem(item);
    };

    const addItem = () => {
        if (item.Product.Name === "" || item.Quantity === 0 || item.Price === 0) {
            setError(true);
        } else {
            handleItem();
        }
    };

    return (
        <Dialog open={ open } onClose={ handleClose } fullWidth={true} maxWidth = {'md'} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Add item</DialogTitle>
            <DialogContent>
                <FormContainer>
                    <RowContainer>
                        <TextField
                            style={{ flexBasis: '600px', paddingRight: '2rem' }}
                            error={ error }
                            margin="dense"
                            id="category"
                            label="Category"
                            type="text"
                            value={ (item.Category.Name !== null && item.Category.Name !== undefined) ? item.Category.Name : "" }
                            disabled
                        />
                        <TextField
                            style={{ flexBasis: '600px' }}
                            error={ error }
                            margin="dense"
                            id="subcategory"
                            label="Subcategory"
                            type="text"
                            value={ (item.Subcategory.Name !== null && item.Subcategory.Name !== undefined) ? item.Subcategory.Name : "" }
                            disabled
                        />
                    </RowContainer>
                    <RowContainer>
                        <FormControl style={{ flexBasis: '600px', paddingRight: '2rem' }} error={ error }>
                            <InputLabel htmlFor="products">Products</InputLabel>
                            <Select
                                autoWidth={ false }
                                value={ (item.Product.Id !== null && item.Product.Id !== undefined) ? item.Product.Id : 0 }
                                onChange={ (e) => {
                                    item.Product = GetInfoById(products, e.target.value);
                                    item.Subcategory = GetInfoById(subcategories, item.Product.ProductSubcategoryID);
                                    item.Category = GetInfoById(categories, item.Subcategory.CategoryId);
                                    updateItem(item);
                                }}
                            >
                                <MenuItem key={ 0 } value={ 0 }> None </MenuItem>
                                {
                                    products && products.map(product => {
                                        return <MenuItem key={ product.Id } value={ product.Id }> { product.Name } </MenuItem>
                                    })
                                }
                            </Select>
                        </FormControl>
                        <TextField
                            style={{ flexBasis: '600px' }}
                            error={ error }
                            id="quantity"
                            label="Quantity"
                            value={ (item.Quantity !== null && item.Quantity !== undefined) ? item.Quantity : 0 }
                            onChange={ changeQuantity }
                            type="number"
                            inputProps={{ min: "0" }}
                        />
                    </RowContainer>
                    <RowContainer>
                        <TextField
                            style={{ flexBasis: '300px' }}
                            error={ error }
                            id="price"
                            label="Price"
                            value={ (item.Price !== null && item.Price !== undefined) ? item.Price : 0 }
                            onChange={ changePrice }
                            type="number"
                            inputProps={{ min: "0", step: "0.01" }}
                        />
                    </RowContainer>
                </FormContainer>
            </DialogContent>
            <DialogActions>
                <Button onClick={ handleClose } color="primary">
                    Cancel
                </Button>
                <Button onClick={ addItem } color="primary">
                    Add
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default AddItem;