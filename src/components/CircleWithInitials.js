import * as React from "react";

const CircleWithInitials = (props) => {
    const {
        size = 75,
        username,
        backgroundColor = "#78849e",
        borderColor = "white",
        color = "white"
    } = props;
    if (!username || username.length < 1) {
        return null;
    }
    const u = username.charAt(0).toUpperCase();
    return (
        <div
            style={{
                width: size,
                height: size,
                borderRadius: size,
                backgroundColor,
                border: "2px solid " + borderColor,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                color,
                fontFamily: "avenir-heavy",
                fontSize: size / 4,
                margin: '0 auto'
            }}
        >
            { u }
        </div>
    );
};

export default CircleWithInitials;