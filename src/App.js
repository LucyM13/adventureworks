import React from 'react';
import Navigation from "./components/Navigation/Navigation";
import { Router } from "@reach/router";
import Route from "./components/Route";
import PrivateRoute from "./components/PrivateRoute";
import Dashboard from "./pages/Dashboard";
import SignIn from "./pages/SignIn";
import SignUp from "./pages/SignUp";
import SignOut from "./pages/SignOut";
import AddCustomer from "./pages/AddCustomer";
import CustomerDetails from "./pages/CustomerDetails";
import AddBill from "./pages/AddBill";
import PageNotFound from "./pages/PageNotFound";

const App = () => {
    return (
      <React.Fragment>
          <Navigation />
          <div className="main">
              <Router>
                  <Route
                      component={ Dashboard }
                      path="/" />
                  <Route exact
                         component={ SignIn }
                         path="/signin" />
                  <Route exact
                         component={ SignUp }
                         path="/signup" />
                  <PrivateRoute exact
                         component={ AddCustomer }
                         path="/customers" />
                  <PrivateRoute
                      component={ CustomerDetails }
                      path="/customers/:id" />
                  <PrivateRoute
                      component={ AddBill }
                      path="/customers/:id/bills" />
                  <PrivateRoute exact
                         component={ SignOut }
                         path="/signout" />
                  <Route
                      component={ PageNotFound }
                      default />
              </Router>
          </div>
      </React.Fragment>
  );
};

export default App;
