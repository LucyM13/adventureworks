import * as React from "react";
import { navigate } from "@reach/router";

class SignOut extends React.Component {

    componentDidMount() {
        localStorage.removeItem("username");
        localStorage.removeItem("token");
        navigate("/");
        window.location.reload();
    }
    render() {
        return null;
    }
};

export default SignOut;