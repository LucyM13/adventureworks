import * as React from "react";

const PageNotFound = () => {
    return (
        <div className="container">
            <h1>Error 404</h1>
            <h2>This page does not exist.</h2>
        </div>
    );
};
export default PageNotFound;