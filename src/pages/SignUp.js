import * as React from "react";
import axios from "axios";
import { navigate } from "@reach/router";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Icon from '@material-ui/core/Icon';
import styled from "styled-components";

const Container = styled.div`
    height: 100vh;
    margin: 2rem;
    display: flex;
    flex-direction: column;
`;

const Title = styled.div`
    min-height: 100;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 3rem;
    margin: auto;
    position: relative;
    left: -5%;
`;

const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 0.5;
    justify-content: center;
`;

const RowContainer = styled.div`
    display: flex;
    padding-bottom: 2rem;
    justify-content: center;
`;

class SignUp extends React.Component {
    state = {
        url: "http://www.fulek.com/nks/api/aw/registeruser",
        name: "",
        username: "",
        password: "",
        loading: false,
        error: false
    };

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.name !== "" && this.state.username !== "" && this.state.password !== "") {
            const user = {
                name: this.state.name,
                username: this.state.username,
                password: this.state.password,
            };

            this.setState({ loading: true, error: false });

            axios.post(this.state.url, user)
                .then(res => {
                    this.setState({ loading: false });
                    console.log(res);
                    console.log(res.data);
                    navigate("/");
                })
                .catch(e => {
                    this.setState({ error: true });
                })
        } else {
            this.setState({ error: true });
        }
    };

    render() {
        return (
            <Container>
                <div style={{ display: 'flex' }}>
                    <Button style={{ width: '5%' }} onClick={ () => navigate("/") }>
                        <Icon fontSize="large" >keyboard_arrow_left</Icon>
                    </Button>
                    <Title>Sign Up</Title>
                </div>
                <FormContainer>
                    <form onSubmit={ this.handleSubmit }>
                        <RowContainer>
                            <TextField
                                style={{ flexBasis: 400 }}
                                label="Name"
                                value={ this.state.name }
                                onChange={ e => {
                                    this.setState({ name: e.target.value });
                                }}
                                required
                                error={ this.state.error }
                            />
                        </RowContainer>
                        <RowContainer>
                            <TextField
                                style={{ flexBasis: 400 }}
                                label="Username"
                                value={ this.state.username }
                                onChange={ e => {
                                    this.setState({ username: e.target.value });
                                }}
                                required
                                error={ this.state.error }
                            />
                        </RowContainer>
                        <RowContainer>
                            <TextField
                                style={{ flexBasis: 400 }}
                                label="Password"
                                type="password"
                                value={ this.state.password }
                                onChange={ e => {
                                    this.setState({ password: e.target.value });
                                }}
                                required
                                error={ this.state.error }
                            />
                        </RowContainer>
                        <RowContainer>
                            <Button
                                variant="outlined"
                                style={{ backgroundColor: "#EB028D", color: "white", width: "2rem", flexBasis: 400 }}
                                onClick={ this.handleSubmit }
                            >
                                Register
                            </Button>
                        </RowContainer>
                    </form>
                </FormContainer>
            </Container>
        );
    }
};

export default SignUp;