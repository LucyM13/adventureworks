import * as React from "react";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { navigate } from "@reach/router";
import styled from "styled-components";

const Container = styled.div`
    height: 100vh;
    margin: 2rem;
    display: flex;
    flex-direction: column;
`;

const Title = styled.div`
    min-height: 100;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 3rem;
`;

const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 0.5;
    justify-content: center;
`;

const RowContainer = styled.div`
    display: flex;
    padding-bottom: 2rem;
    justify-content: center;
`;

class SignIn extends React.Component {
    state = {
        url: "http://www.fulek.com/nks/api/aw/login",
        username: "",
        password: "",
        error: false,
    };

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.username !== "" && this.state.password !== "") {
            const user = {
                username: this.state.username,
                password: this.state.password,
            };

            this.setState({ error: false });

            axios.post(this.state.url, user)
                .then(res => {
                    if (res.data && res.data.token) {
                        localStorage.setItem("username", res.data.username);
                        localStorage.setItem("token", res.data.token);
                    }
                    navigate("/");
                    window.location.reload();
                })
                .catch(e => {
                    this.setState({ error: true });
                })
        } else {
            this.setState({ error: true });
        }

    };

    render() {
        return (
            <Container>
                <Title> Sign In </Title>
                <FormContainer>
                    <form>
                        <RowContainer>
                            <TextField
                                style={{ flexBasis: 400 }}
                                label="Username"
                                value={ this.state.username }
                                onChange={ e => {
                                    this.setState({ username: e.target.value });
                                }}
                                required
                                error={ this.state.error }
                            />
                        </RowContainer>
                        <RowContainer>
                            <TextField
                                style={{ flexBasis: 400 }}
                                label="Password"
                                type="password"
                                value={ this.state.password }
                                onChange={ e => {
                                    this.setState({ password: e.target.value });
                                }}
                                required
                                error={ this.state.error }
                            />
                        </RowContainer>
                        <RowContainer/>
                        <RowContainer>
                            <Button
                                style={{ backgroundColor: "#EB028D", color: "white", width: "2rem", flexBasis: 400 }}
                                onClick={ this.handleSubmit }
                            >
                                Sign In
                            </Button>
                        </RowContainer>
                        <RowContainer>
                            <Button
                                style={{ backgroundColor: "#3B5998", color: "white", width: "2rem", flexBasis: 400 }}
                                onClick={ () => {
                                    navigate("/signup");
                                }}
                            >
                                Sign Up
                            </Button>
                        </RowContainer>
                    </form>
                </FormContainer>
            </Container>
        );
    }
};

export default SignIn;