import * as React from "react";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import EnhancedTableHead from "../components/TableGeneric/EnhancedTableHead";
import styled from "styled-components";
import Pagination from "../components/TableGeneric/Pagination";
import GetConnectedUser from "../components/Utils/GetConnectedUser";
import GetInfoById from "../components/Utils/GetInfoById";
import { navigate } from "@reach/router";

const Container = styled.div`
    height: 100vh;
    margin: 2rem;
    display: flex;
    flex-direction: column;
`;

const Title = styled.div`
    min-height: 100;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 3rem;
    padding-bottom: 1rem;
`;

const RowContainer = styled.div`
    padding-top: 2rem;
    padding-bottom: 2rem;
`;

const headRows = [
    { id: 'name', label: 'Name' },
    { id: 'surname', label: 'Surname' },
    { id: 'email', label: 'Email' },
    { id: 'telephone', label: 'Telephone' },
    { id: 'city', label: 'City' },
    { id: 'state', label: 'State' }
];

class Dashboard extends React.Component {

    state = {
        urlCustomers: "http://www.fulek.com/nks/api/aw/customers",
        urlCities: "http://www.fulek.com/nks/api/aw/cities",
        urlStates: "http://www.fulek.com/nks/api/aw/states",
        data: [],
        order: "asc",
        orderBy: "",
        page: 0,
        rowsPerPage: 10,
        searchQuery: "",
        cities: [],
        states: []
    };

    /** Load data for customers, cities and states **/

    componentDidMount() {

        axios.all([
            axios.get(this.state.urlCustomers),
            axios.get(this.state.urlCities),
            axios.get(this.state.urlStates)
        ])
        .then(axios.spread((customersResponse, citiesResponse, statesResponse) => {

            let customers = customersResponse.data;
            customers !== undefined && customers.map(customer => {
                const city = GetInfoById(citiesResponse.data, customer.CityId);
                if (city !== undefined) {
                    customer["City"] = city.Name;
                    const state = GetInfoById(statesResponse.data, city.StateId);
                    if (state !== undefined)
                        customer["State"] = state.Name;
                }
                return customer;
            });
            this.setState({ data: customers, cities: citiesResponse.data, states: statesResponse.data })
        }))
    }

    /** Handle Sorting Data **/

    sorting = () => {
        if (this.state.searchQuery === "")
            return this.sortByAscDesc(this.state.data, this.getSorting(this.state.order, this.state.orderBy));
        else
            return this.sortByQuery(this.state.data, this.state.searchQuery);
    };

    handleRequestSort = (event, property) => {
        const isDesc = this.state.orderBy === (property.charAt(0).toUpperCase() + property.substring(1).toLowerCase()) && this.state.order === 'desc';
        this.setState({ order: isDesc ? 'asc' : 'desc' });
        this.setState({ orderBy: property.charAt(0).toUpperCase() + property.substring(1).toLowerCase() });
    };

    desc = (a, b, orderBy) => {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    };

    sortByQuery = (array, query) => {
        return array.filter(row => {
            const array = Object.values(row).filter((value, index, arr) => {
                return typeof value === 'string';
            });
            return array.find((el, index) => {
                return el.toLowerCase().startsWith(query.toLowerCase());
            });
        });
    };

    sortByAscDesc = (array, cmp) => {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
            const order = cmp(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis.map(el => el[0]);
    };

    getSorting = (order, orderBy) => {
        return order === 'desc' ? (a, b) => this.desc(a, b, orderBy) : (a, b) => -this.desc(a, b, orderBy);
    };

    /** Handle Pagination **/

    handleChangePage = (event, newPage) => {
        this.setState({ page: newPage });
    };

    handleChangeRowsPerPage = (event) => {
        this.setState({ rowsPerPage: event.target.value });
    };

    render() {
        return (
            <Container>
                <Title>Adventure Works</Title>
                <RowContainer>
                    {
                        GetConnectedUser() === null &&
                        <TextField
                            style={{ justifyContent: 'center', width: '100%' }}
                            label="Search"
                            type="search"
                            onChange={ (e) => {
                                this.setState({ searchQuery: e.target.value });
                            }}
                        />
                    }
                    {
                        GetConnectedUser() !== null &&
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <TextField
                                style={{ display: 'flex', flexBasis: "45%" }}
                                label="Search"
                                type="search"
                                onChange={ (e) => {
                                    this.setState({ searchQuery: e.target.value });
                                }}
                            />
                            <Button
                                style={{ display: 'flex', backgroundColor: "#EB028D", color: "white", flexBasis: "45%" }}
                                onClick={
                                    () => {
                                        navigate("/customers");
                                    }
                                }
                            >
                                Add customer
                            </Button>
                        </div>
                    }
                </RowContainer>
                <Table>
                    <EnhancedTableHead
                        headRows={ headRows }
                        order={ this.state.order }
                        orderBy={ this.state.orderBy }
                        onRequestSort={ this.handleRequestSort }
                    />
                    <TableBody>
                        {
                            this.state.data &&
                            this.sorting()
                            .slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                            .map(customer => {
                                return (
                                    <TableRow
                                        hover
                                        key={ customer.Id }
                                        onClick={ () => {
                                            if (GetConnectedUser() !== null)
                                                navigate("/customers/" + customer.Id, { state: {
                                                        customer: customer,
                                                        cities: this.state.cities,
                                                        states: this.state.states
                                                    }
                                                });
                                        }}
                                    >
                                        <TableCell component="th" scope="row">
                                            { customer.Name }
                                        </TableCell>
                                        <TableCell>{ customer.Surname }</TableCell>
                                        <TableCell>{ customer.Email }</TableCell>
                                        <TableCell>{ customer.Telephone }</TableCell>
                                        <TableCell>{ customer.City }</TableCell>
                                        <TableCell>{ customer.State }</TableCell>
                                    </TableRow>
                                )
                            })
                        }
                    </TableBody>
                </Table>
                <Pagination
                    rowsPerPageOptions={ [10, 20, 50] }
                    count={ this.state.data.length }
                    rowsPerPage={ this.state.rowsPerPage }
                    page={ this.state.page }
                    onChangePage={ this.handleChangePage }
                    onChangeRowsPerPage={ this.handleChangeRowsPerPage }
                />
            </Container>
        );
    }
};

export default Dashboard;