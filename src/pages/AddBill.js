import * as React from "react";
import axios from "axios";
import styled from "styled-components";
import {navigate} from "@reach/router";
import Icon from '@material-ui/core/Icon';
import Button from "@material-ui/core/Button";
import BillForm from "../components/BillForm";
import GetCurrentDate from "../components/Utils/GetCurrentDate";

const Container = styled.div`
    height: 100vh;
    margin: 2rem !important;
    display: flex;
    flex-direction: column;
`;

const Title = styled.div`
    min-height: 100;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 3rem;
    padding-bottom: 1rem;
    margin: auto;
    position: relative;
    left: -5%;
`;

class AddBill extends React.Component {
    state = {
        config: {
            Authorization: 'Bearer ' + localStorage.getItem("token"),
            'Content-Type': 'application/json',
        },
        urlAddBill: "http://www.fulek.com/nks/api/aw/addbill",
        urlAddItem: "http://www.fulek.com/nks/api/aw/additem",
        urlGetSellers: "http://www.fulek.com/nks/api/aw/sellers",
        urlGetProduct: "http://www.fulek.com/nks/api/aw/products/",
        urlGetSubcategory: "http://www.fulek.com/nks/api/aw/subcategories/",
        urlGetCategories: "http://www.fulek.com/nks/api/aw/categories/",
        bill: {
            BillNumber: "",
            CustomerId: this.props.id,
            SellerId: 0,
            Items: []
        },
        item: {
            Id: 0,
            Quantity: 0,
            Product: {
                Id: 0,
                Name: ""
            },
            Subcategory: {
                Id: 0,
                Name: ""
            },
            Category: {
                Id: 0,
                Name: ""
            },
            Price: 0
        },
        error: false,
        sellers: [],
        categories: [],
        subcategories: [],
        products: []
    };

    componentDidMount() {
        axios.all([
            axios.get(this.state.urlGetSellers),
            axios.get(this.state.urlGetCategories)
        ])
            .then(axios.spread((sellersResponse, categoriesResponse) => {
                this.setState({sellers: sellersResponse.data, categories: categoriesResponse.data});

                let data = [];
                this.state.categories &&
                axios.all(this.state.categories.map(category => {
                    return axios.get(this.state.urlGetSubcategory + category.Id)
                }))
                    .then(axios.spread((...responses) => {
                        responses.map(res => {
                            return res.data.map(el => {
                                return data.push(el);
                            })
                        });
                        this.setState({ subcategories: data });
                        data = [];
                        axios.all(this.state.subcategories.map(subcategory => {
                            return axios.get(this.state.urlGetProduct + subcategory.Id)
                        }))
                            .then(axios.spread((...responses) => {
                                responses.map(res => {
                                    return res.data.map(el => {
                                        return data.push(el);
                                    })
                                });
                                this.setState({products: data});

                            }));
                    }))
            }))
    }

    updateBill = (bill) => {
        this.setState( prevState => ({
            bill: {
                ...prevState.bill,
                BillNumber: bill.BillNumber,
                SellerId: bill.SellerId,
                Items: bill.Items
            }
        }));
    };

    updateItemsFromBill = (items) => {
        this.setState( prevState => ({
            bill: {
                ...prevState.bill,
                Items: items
            }
        }));
    };


    updateItem = (item) => {
        this.setState( prevState => ({
            item: {
                ...prevState.item,
                Quantity: item.Quantity,
                Product: item.Product,
                Subcategory: item.Subcategory,
                Category: item.Category,
                Price: item.Price
            }
        }));
    };

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.bill.BillNumber !== "" && this.state.bill.SellerId !== 0
            && this.state.bill.Items.length !== 0) {

            this.setState({ error: false });

            const bill = {
                Date: GetCurrentDate(),
                BillNumber: this.state.bill.BillNumber,
                SellerId: this.state.bill.SellerId,
                CustomerId: this.state.bill.CustomerId,
                CreditCardId: 9458,
                Comment: ""
            };

            console.log(bill);
            axios.post(this.state.urlAddBill, bill, {
                headers: this.state.config,
            })
                .then(res => {
                    axios.all(this.state.bill.Items.map(itemStored => {
                        const item = {
                            BillId: res.data.Id,
                            Quantity: itemStored.Quantity,
                            ProductId: itemStored.Product.Id,
                            TotalPrice: itemStored.Price
                        };
                        return axios.post(this.state.urlAddItem, item, {
                            headers: this.state.config,
                        })
                    }))
                        .then(axios.spread((...responses) => {

                            navigate("/");
                        }))
                })
                .catch(e => {
                    this.setState({ error: true });
                })
        } else {
            this.setState({ error: true });
        }

    };

    handleDeleteItem = (itemToDelete) => {
        const items = this.state.bill.Items;
        const i = items.findIndex(item => {
            return (item.Id === itemToDelete.Id)
        });
        if (i !== -1) {
            items.splice(i, 1);
            this.updateItemsFromBill(items);
        }
    };

    render() {
        return (
            <Container>
                <div style={{ display: 'flex' }}>
                    <Button style={{ width: '5%' }} onClick={ () => navigate("/customers/" + this.state.bill.CustomerId) }>
                        <Icon fontSize="large" >keyboard_arrow_left</Icon>
                    </Button>
                    <Title>Add new bill</Title>
                </div>
                <BillForm
                    error={ this.state.error }
                    bill={ this.state.bill }
                    sellers={ this.state.sellers }
                    categories={ this.state.categories }
                    subcategories={ this.state.subcategories }
                    products={ this.state.products }
                    item={ this.state.item }
                    updateItem={ this.updateItem }
                    updateBill={ this.updateBill }
                    handleDeleteItem={ this.handleDeleteItem }
                    handleSubmit={ this.handleSubmit } />
            </Container>
        );
    }
}

export default AddBill;