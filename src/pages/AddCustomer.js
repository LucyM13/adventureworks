import * as React from "react";
import axios from "axios";
import styled from "styled-components";
import {navigate} from "@reach/router";
import Icon from '@material-ui/core/Icon';
import Button from "@material-ui/core/Button";
import CustomerForm from "../components/CustomerForm";

const Container = styled.div`
    height: 100vh;
    margin: 2rem !important;
    display: flex;
    flex-direction: column;
`;

const Title = styled.div`
    min-height: 100;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 3rem;
    padding-bottom: 1rem;
    margin: auto;
    position: relative;
    left: -5%;
`;

class AddCustomer extends React.Component {
    state = {
        config: {
            Authorization: 'Bearer ' + localStorage.getItem("token"),
            'Content-Type': 'application/json',
        },
        urlAddCustomer: "http://www.fulek.com/nks/api/aw/addcustomer",
        urlCities: "http://www.fulek.com/nks/api/aw/cities",
        urlStates: "http://www.fulek.com/nks/api/aw/states",
        customer: {
            Name: "",
            Surname: "",
            Email: "",
            Telephone: "",
            CityId: 0,
            City: "",
            State: "",
        },
        error: false,
        cities: [],
        states: []
    };

    componentDidMount() {
        axios.all([
            axios.get(this.state.urlCities),
            axios.get(this.state.urlStates)
        ])
            .then(axios.spread((citiesResponse, statesResponse) => {
                this.setState({ cities: citiesResponse.data, states: statesResponse.data });
            }))
    }

    updateCustomer = (customer) => {
        this.setState( prevState => ({
            customer: {
                ...prevState.customer,
                Name: customer.Name,
                Surname: customer.Surname,
                Email: customer.Email,
                Telephone: customer.Telephone,
                CityId: customer.CityId,
                State: customer.State
            }
        }));
    };


    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.customer.Name !== "" && this.state.customer.Surname !== ""
            && this.state.customer.Email !== "" && this.state.customer.Telephone !== ""
            && this.state.customer.CityId !== 0) {

            this.setState({ error: false });

            const customer = {
                Name: this.state.customer.Name,
                Surname: this.state.customer.Surname,
                Email: this.state.customer.Email,
                Telephone: this.state.customer.Telephone,
                CityId: this.state.customer.CityId
            };

            axios.post(this.state.urlAddCustomer, customer, {
                headers: this.state.config,
            })
                .then(res => {
                    navigate("/");
                })
                .catch(e => {
                    this.setState({ error: true });
                })
        } else {
            this.setState({ error: true });
        }

    };

    render() {
        return (
            <Container>
                <div style={{ display: 'flex' }}>
                    <Button style={{ width: '5%' }} onClick={ () => navigate("/") }>
                        <Icon fontSize="large" >keyboard_arrow_left</Icon>
                    </Button>
                    <Title>Add new customer</Title>
                </div>
                <CustomerForm
                    error={ this.state.error }
                    customer={ this.state.customer }
                    cities={ this.state.cities }
                    states={ this.state.states }
                    updateCustomer={ this.updateCustomer }
                    handleSubmit={ this.handleSubmit }
                    handleDelete={ null } />
            </Container>
        );
    }
}

export default AddCustomer;