import * as React from "react";
import axios from "axios";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Button from "@material-ui/core/Button";
import Icon from '@material-ui/core/Icon';
import styled from "styled-components";
import "../components/Navigation/Navigation.css";
import Information from "../fragments/CustomerDetails/Information";
import Bill from "../fragments/CustomerDetails/Bill";
import GetInfoById from "../components/Utils/GetInfoById";
import FillInfoCustomer from "../components/Utils/FillInfoCustomer";
import { navigate } from "@reach/router";

const Container = styled.div`
    height: 100vh;
    margin: 2rem !important;
    display: flex;
    flex-direction: column;
`;

const Title = styled.div`
    min-height: 100;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 3rem;
    padding-bottom: 1rem;
    margin: auto;
    position: relative;
    left: -5%;
`;

const TabContainer = styled.div`
    padding-top: 2rem;
`;

class CustomerDetails extends React.Component {

    state = {
        urlGetCustomers: "http://www.fulek.com/nks/api/aw/customers/",
        urlGetCities: "http://www.fulek.com/nks/api/aw/cities/",
        urlGetStates: "http://www.fulek.com/nks/api/aw/states/",
        urlGetBills: "http://www.fulek.com/nks/api/aw/customerbills/",
        urlGetItems: "http://www.fulek.com/nks/api/aw/billitems/",
        urlGetSubcategory: "http://www.fulek.com/nks/api/aw/subcategories/",
        urlGetCategories: "http://www.fulek.com/nks/api/aw/categories/", // API ne propose que de get only all category

        tabValue: 0,
        expanded: "",

        customer: {
            Id: this.props.id,
            Name: (this.props.location.state.customer) ? this.props.location.state.customer.Name : "",
            Surname: (this.props.location.state.customer) ? this.props.location.state.customer.Surname : "",
            Email: (this.props.location.state.customer) ? this.props.location.state.customer.Email : "",
            Telephone: (this.props.location.state.customer) ? this.props.location.state.customer.Telephone : "",
            State: (this.props.location.state.customer) ? this.props.location.state.customer.State : "",
            City: (this.props.location.state.customer) ? this.props.location.state.customer.City : "",
            CityId: (this.props.location.state.customer) ? this.props.location.state.customer.CityId : 0
        },
        cities: (this.props.location.state.cities) ? this.props.location.state.cities : [],
        states: (this.props.location.state.states) ? this.props.location.state.states : [],
        bills: [],
        items: [],
        categories: [],
    };

    componentDidMount() {

        const fillInfoBills = () => {
            let arr = [];
            this.state.bills && this.state.bills.length !== 0 &&
            this.state.bills.map(bill => {
                return axios.get(this.state.urlGetItems + bill.Id)
                    .then(res => {
                        res.data.map(item => {
                            return arr.push(item);
                        });
                        this.setState({ items: arr });

                        this.state.items && this.state.items.map(item => {
                            return axios.get(this.state.urlGetSubcategory + item.Product.ProductSubcategoryID)
                                .then(res => {
                                    item.Product["Subcategory"] = res.data;
                                    item.Product["Category"] = (item.Product.Subcategory.length !== 0) ? GetInfoById(this.state.categories, item.Product.Subcategory[0].CategoryId) : "";
                                });
                        })
                    })
            });
        };

        const getBills = () => {
            axios.get(this.state.urlGetBills + this.props.id)
                .then(res => {
                    this.setState({ bills: res.data });
                    fillInfoBills();
                })
                .catch(e => console.log)
        };

        if (this.props.location.state.customer === undefined && this.props.location.state.cities === undefined && this.props.location.state.states === undefined) {
            axios.all([
                axios.get(this.state.urlGetCustomers),
                axios.get(this.state.urlGetCities),
                axios.get(this.state.urlGetStates),
                axios.get(this.state.urlGetCategories)
            ])
                .then(axios.spread((customersResponse, citiesResponse, statesResponse, categoriesResponse) => {
                    const customer = FillInfoCustomer(this.props.id, customersResponse.data, citiesResponse.data, statesResponse.data);
                    this.setState({
                        customer: customer,
                        categories: categoriesResponse.data,
                        cities: citiesResponse.data,
                        states: statesResponse.data
                    });
                    getBills();
                }));
        } else {
            getBills();
        }
    }

    handleChange = (event, newValue) => {
        this.setState({ tabValue: newValue });
    };


    render() {
        return (
            <Container>
                <div style={{ display: 'flex' }}>
                    <Button style={{ width: '5%' }} onClick={ () => navigate("/") }>
                        <Icon fontSize="large" >keyboard_arrow_left</Icon>
                    </Button>
                    <Title>Customer #{ this.state.customer.Id }</Title>
                </div>
                <AppBar position="static" style={{ backgroundColor: "white", color: "#2A2E43" }}>
                    <Tabs value={ this.state.tabValue } onChange={ this.handleChange }>
                        <Tab label="Information" style={{ flexGrow: 400 }} />
                        <Tab label="Bills" style={{ width: "100%" }} />
                    </Tabs>
                </AppBar>
                {
                    this.state.tabValue === 0 &&
                    <TabContainer>
                        {
                            this.state.customer &&
                            this.state.customer.Name !== "" && this.state.customer.Surname !== "" &&
                            this.state.customer.Telephone !== "" && this.state.customer.Email !== "" &&
                            this.state.customer.CityId !== 0 &&
                            <Information customer={ this.state.customer } cities={ this.state.cities } states={ this.state.states } />
                        }
                    </TabContainer>
                }
                {
                    this.state.tabValue === 1 &&
                    <TabContainer>
                        <div style={{ display: 'flex', justifyContent: 'center', padding: '1rem' }}>
                            <Button
                                style={{ display: 'flex', backgroundColor: "#EB028D", color: "white", flexBasis: "50%" }}
                                onClick={ () => {
                                        navigate("/customers/" + this.state.customer.Id + "/bills");
                                    }
                                }
                            >
                                Add bill
                            </Button>
                        </div>
                        {
                            this.state.bills && this.state.bills.Id !== "" &&
                            this.state.bills.map(bill => {
                                const items = this.state.items.filter(item => {
                                    return (item.BillId === bill.Id);
                                });
                                return <Bill key={ bill.Id } bill={ bill } items={ items } categories={ this.state.categories }/>
                            })
                        }
                    </TabContainer>
                }
            </Container>
        );
    }
};

export default CustomerDetails;