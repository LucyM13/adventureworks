import * as React from "react";

const PageNotAuthorized = () => {
    return (
        <div className="container">
            <h1>Error 403</h1>
            <h2>You are not authorized to view this page.</h2>
        </div>
    );
};
export default PageNotAuthorized;