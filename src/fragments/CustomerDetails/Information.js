import * as React from "react";
import axios from "axios/index";
import styled from "styled-components";
import {navigate} from "@reach/router";
import CustomerForm from "../../components/CustomerForm";

const Container = styled.div`
    height: 100vh;
    margin: 2rem !important;
    display: flex;
    flex-direction: column;
`;

class Information extends React.Component {

    state = {
        urlEditCustomer: "http://www.fulek.com/nks/api/aw/editcustomer/",
        urlDeleteCustomer:"http://www.fulek.com/nks/api/aw/deletecustomer/",
        customer: {
            Id: this.props.customer.Id,
            Name: this.props.customer.Name,
            Surname: this.props.customer.Surname,
            Email: this.props.customer.Email,
            Telephone: this.props.customer.Telephone,
            CityId: this.props.customer.CityId,
            State: this.props.customer.State,
        },
        cities: this.props.cities,
        states: this.props.states,
        error: false,
        config: {
            Authorization: 'Bearer ' + localStorage.getItem("token"),
            'Content-Type': 'application/json'
        }
    };

    updateCustomer = (customer) => {
        this.setState( prevState => ({
            customer: {
                ...prevState.customer,
                Name: customer.Name,
                Surname: customer.Surname,
                Email: customer.Email,
                Telephone: customer.Telephone,
                CityId: customer.CityId,
                State: customer.State
            }
        }));
    };

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.customer.Name !== "" && this.state.customer.Surname !== "" && this.state.customer.Email !== "" && this.state.customer.Telephone !== "") {

            this.setState({ error: false });
            const customer = {
                Id: this.state.customer.Id,
                Name: this.state.customer.Name,
                Surname: this.state.customer.Surname,
                Email: this.state.customer.Email,
                Telephone: this.state.customer.Telephone,
                CityId: this.state.customer.CityId
            };
            axios.post(this.state.urlEditCustomer, customer, {
                headers: this.state.config,
            })
                .then(res => {
                    navigate("/");
                })
                .catch(e => {
                    this.setState({ error: true });
                })
        } else {
            this.setState({ error: true });
        }

    };

    handleDelete = () => {
        axios.post(this.state.urlDeleteCustomer, { 'Id': this.state.customer.Id }, {
            headers: this.state.config
        })
            .then(res => {
                navigate("/");
            })
    };
    render () {
        return (
            <Container>
                <CustomerForm
                    error={ this.state.error }
                    customer={ this.state.customer }
                    cities={ this.state.cities }
                    states={ this.state.states }
                    updateCustomer={ this.updateCustomer }
                    handleSubmit={ this.handleSubmit }
                    handleDelete={ this.handleDelete } />
            </Container>
        );
    }
};

export default Information;