import * as React from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

class Bills extends React.Component {

    state = {
        items: this.props.items,
        bill: {
            Id: this.props.bill.Id,
            Date: this.props.bill.Date,
            BillNumber: this.props.bill.BillNumber,
            SellerId: this.props.bill.SellerId,
            Total: 0
        }
    };

    componentDidMount() {
        let total = 0;
        this.state.items && this.state.items.map(item => {
            return (total += item.TotalPrice);
        });
        this.updateTotal(total);
    }

    updateTotal = (total) => {
        this.setState( prevState => ({
            bill: {
                ...prevState.bill,
                Total: total
            }
        }));
    };

    handleChange = panel => (event, newExpanded) => {
        this.setState({ expanded: newExpanded ? panel : false });
    };

    render () {
        return (
            <ExpansionPanel key={ this.state.bill.Id }
                            square expanded={ this.state.expanded === this.state.bill.Id }
                            onChange={ this.handleChange(this.state.bill.Id) }>
                <ExpansionPanelSummary>
                    Bill #{ this.state.bill.Id }
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Table>
                       <TableHead>
                           <TableRow>
                               <TableCell>Category</TableCell>
                               <TableCell>SubCategory</TableCell>
                               <TableCell>Products</TableCell>
                               <TableCell>Quantity</TableCell>
                               <TableCell align="right">Price (HRK)</TableCell>
                           </TableRow>
                       </TableHead>
                       <TableBody>
                           {
                               this.state.items && this.state.items.map(item => {
                                   return (
                                       <TableRow key={ item.Id }>
                                           <TableCell component="th" scope="row">
                                               { (item.Product.Category !== undefined) ? item.Product.Category.Name : "" }
                                           </TableCell>
                                           <TableCell>
                                               { item.Product.Subcategory.map(subcategory => {
                                                   return <span key={ subcategory.Id }> [{ subcategory.Name }] </span>;
                                               }) }
                                           </TableCell>
                                           <TableCell>{ (item.Product.Name !== undefined) ? item.Product.Name : "" }</TableCell>
                                           <TableCell>{ (item.Quantity !== undefined) ? item.Quantity : 0 }</TableCell>
                                           <TableCell align="right">{ (item.TotalPrice !== undefined) ? item.TotalPrice : 0 } HRK</TableCell>
                                       </TableRow>
                                   );
                               })
                           }
                           <TableRow>
                               <TableCell colSpan={ 4 }><strong>Total</strong></TableCell>
                               <TableCell align="right"><strong>{ this.state.bill.Total } HRF</strong></TableCell>
                           </TableRow>
                       </TableBody>
                   </Table>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
};

export default Bills;